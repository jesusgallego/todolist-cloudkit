//
//  TodayViewController.swift
//  ToDoListWidget
//
//  Created by Master Móviles on 7/4/17.
//
//

import UIKit
import NotificationCenter

class TodayViewController: UIViewController, NCWidgetProviding {
        
    @IBOutlet weak var numItemsLabel: UILabel!
    @IBOutlet weak var lastActionLabel: UILabel!
    
    var shared: UserDefaults!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view from its nib.
        
        shared = UserDefaults(suiteName: "group.ua.mastermoviles.ToDoList")
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        // Perform any setup necessary in order to update the view.
        
        // If an error is encountered, use NCUpdateResult.Failed
        // If there's no update required, use NCUpdateResult.NoData
        // If there's an update, use NCUpdateResult.NewData
        
        shared.synchronize()
        
        numItemsLabel.text = "\(shared.integer(forKey: "counter"))"
        lastActionLabel.text = shared.string(forKey: "last-item");
        
        completionHandler(NCUpdateResult.newData)
    }
    
}
